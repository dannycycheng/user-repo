package com.laughoutloud.user.repo.core.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "users")
public class User {

    @Id
    private String id;

    private String firstName;

    private String lastName;

    private Map<String, String> attributes;

    public User() {
        super();
    }

    /**
     * Creates a new {@link User} from the given first name, last name and attributes.
     *
     * @param firstName must not be null or empty.
     */
    public User(String firstName, String lastName, Map<String, String> attributes) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return String.format("User[id=%s, firstName=%s, lastName=%s]", id, firstName, lastName);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }
}
