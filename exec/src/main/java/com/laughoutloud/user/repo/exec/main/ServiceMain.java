package com.laughoutloud.user.repo.exec.main;

import com.laughoutloud.user.repo.app.persistence.UserRepository;
import com.laughoutloud.user.repo.iadp.config.IadpConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackageClasses = UserRepository.class)
@Import({IadpConfig.class})
public class ServiceMain {
    public static void main(String[] args) {
        SpringApplication.run(ServiceMain.class, args);
    }
}