package com.laughoutloud.user.repo.iadp.controller;

import com.laughoutloud.user.repo.app.boundary.UserManager;
import com.laughoutloud.user.repo.app.model.UserListRepresentation;
import com.laughoutloud.user.repo.app.model.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;

@RestController
public class UserRepoController {

    @Autowired
    private UserManager userManager;

    @RequestMapping(method = RequestMethod.GET, path = "/user-repo")
    public UserListRepresentation getUsers(@RequestParam(name = "firstName", required = false) String firstName) {
        if (firstName == null || firstName.isEmpty()) {
            return userManager.findAllUsers();
        } else {
            return userManager.findAllUsersWithFirstName(firstName);
        }
    }

    @RequestMapping(method = RequestMethod.POST, path = "/user-repo")
    public ResponseEntity<UserRepresentation> addUser(@Validated @RequestBody UserRepresentation request) {
        userManager.addUser(request);
        return ResponseEntity.created(UserRepoLinkExtender.single(request)).body(request);
    }
}
