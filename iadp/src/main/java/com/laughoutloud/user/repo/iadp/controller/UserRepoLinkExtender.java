package com.laughoutloud.user.repo.iadp.controller;

import com.laughoutloud.user.repo.app.model.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.Link;

import java.net.URI;
import java.net.URISyntaxException;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class UserRepoLinkExtender {

    private static final Logger logger = LoggerFactory.getLogger(UserRepoLinkExtender.class);

    public static URI single(UserRepresentation userRepresentation) {
        try {
            Link link = linkTo(methodOn(UserRepoController.class).addUser(userRepresentation)).withSelfRel();
            return new URI(link.getHref());
        } catch (URISyntaxException e) {
            logger.error("Unable to construct self link");
            return null;
        }
    }
}
