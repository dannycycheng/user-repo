package com.laughoutloud.user.repo.iadp.config;

import com.laughoutloud.user.repo.app.config.AppConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.laughoutloud.user.repo.iadp")
@Import({AppConfig.class})
public class IadpConfig {
}
