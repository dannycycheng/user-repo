package com.laughoutloud.user.repo.app.model;

import com.laughoutloud.user.repo.core.domain.User;

import java.util.List;

public class RepresentationCreator {

    public static UserRepresentation toRepresentation(User user) {
        UserRepresentation result = new UserRepresentation();
        result.setFirstName(user.getFirstName());
        result.setLastName(user.getLastName());
        result.setAttributes(user.getAttributes());

        return result;
    }

    public static UserListRepresentation toRepresentation(List<User> users) {
        UserListRepresentation result = new UserListRepresentation();

        for (User user : users) {
            UserRepresentation userRep = new UserRepresentation();
            userRep.setFirstName(user.getFirstName());
            userRep.setLastName(user.getLastName());
            userRep.setAttributes(user.getAttributes());

            result.getUserList().add(userRep);
        }

        return result;
    }
}
