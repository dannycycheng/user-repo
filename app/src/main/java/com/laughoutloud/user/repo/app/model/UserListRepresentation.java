package com.laughoutloud.user.repo.app.model;

import java.util.HashSet;
import java.util.Set;

public class UserListRepresentation {
    private Set<UserRepresentation> userList = new HashSet<UserRepresentation>();

    public Set<UserRepresentation> getUserList() {
        return userList;
    }

    public void setUserList(Set<UserRepresentation> userList) {
        this.userList = userList;
    }
}
